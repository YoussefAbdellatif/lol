package XMLPack;

import UsersPack.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class LoadXML {

    XMLRestaurant Restaurant = new XMLRestaurant();
    File source = new File("main.xml");
    JAXBContext jaxbContext;
    {
        try {
            jaxbContext = JAXBContext.newInstance(XMLRestaurant.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Restaurant = (XMLRestaurant) jaxbUnmarshaller.unmarshal(source);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public List<User> LoadUsers(){
        
        List<User> Users = new ArrayList<>();

        for (XMLUser xmluser: Restaurant.getXMLUsers().getXMLUsers() ) {
            User user;
            if (xmluser.getRole().equals("Client")){
                user = new Client();
            }else if (xmluser.getRole().equals("Manager")){
                user = new Manager();
            }else if (xmluser.getRole().equals("Waiter")){
                user = new Waiter();
            }else if (xmluser.getRole().equals("Cooker")){
                user = new Chef();
            }else { user = null; }
            Users.add(user);
        }

        return Users;
    }

}
