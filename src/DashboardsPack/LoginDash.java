package DashboardsPack;

import MainPack.Restaurant;
import UsersPack.Client;
import UsersPack.User;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginDash {
    @FXML
    Button button;
    @FXML
    TextField username;
    @FXML
    PasswordField password;
    @FXML
    ImageView imageView;

    public void Login(javafx.event.ActionEvent event) throws Exception {
        Restaurant Restaurant = MainPack.Restaurant.getInstance();
        ///Image image = new Image("/Icons/LoginPhoto.jpg");
        //  imageView.setImage(image);
        boolean found = false;
        for (User user : Restaurant.getUsers()) {
            if (user.getUsername().equals(username.getText())) {
                if (user.getPassword().equals(password.getText())) {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource(user.login()));
                    Parent parent = loader.load();
                    Scene Dashboard = new Scene(parent);
                    ClientDash clientDash = loader.getController();
                    clientDash.details((Client) user);
                    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    stage.setScene(Dashboard);
                    stage.show();

                } else {
                    found = true;
                    //*TODO:IF ONLY USERNAME IS CORRECT*/
                }
                break;
            }
        }
        if (found) ;

        else {
            /*TODO: USER NOT FOUND*/
            username.clear();
            password.clear();
        }


    }


}
