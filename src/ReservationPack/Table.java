package ReservationPack;

public class Table extends ReserveItem{

    private int number;
    private int numberOfSeats;
    private boolean smoking;
    public enum Location{
        Indoor,
        Outdoor
    }
    private Location location;

    public void setNumber(int number) {
        this.number = number;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
