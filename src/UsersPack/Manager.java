package UsersPack;

public class Manager extends Employee {
    public Manager(String firstName, String lastName, String username, String password) {
        super(firstName, lastName, username, password);
    }

    public Manager() {
        super();
    }
}
