package UsersPack;

public abstract class User {

    //Attributes
    private String firstName;
    private String lastName;
    private String username;
    private String password;

    //Constructors

    public User(String firstName, String lastName, String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
    }

    public User() {

    }

    public void login(){

    }
    public void logout(){

    }

}
