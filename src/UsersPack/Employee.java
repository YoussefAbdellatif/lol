package UsersPack;

public abstract class Employee extends User{

    public Employee(String firstName, String lastName, String username, String password) {
        super(firstName, lastName, username, password);
    }

    public Employee() {
        super();
    }
}
